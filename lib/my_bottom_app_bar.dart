import 'package:flutter/material.dart';

import 'main.dart';

class MyBottomAppBar extends StatelessWidget {
  const MyBottomAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      notchMargin: 4,
      color: const Color.fromRGBO(179, 229, 252, 1),
      elevation: 10,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 62,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              child: MaterialButton(
                  onPressed: () {},
                  minWidth: 24,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(Icons.home),
                    ],
                  )),
            ),
            SizedBox(
              child: MaterialButton(
                  onPressed: () {},
                  minWidth: 24,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      ImageIcon(
                        AssetImage("images/img1.png"),
                        size: 24,
                      ),
                    ],
                  )),
            ),
            SizedBox(
              child: MaterialButton(
                  onPressed: () {},
                  minWidth: 24,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.euro,
                        size: 24,
                      ),
                    ],
                  )),
            ),
            SizedBox(
              child: MaterialButton(
                  onPressed: () {},
                  minWidth: 24,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.info,
                        size: 24,
                      )
                    ],
                  )),
            ),
            SizedBox(
              child: MaterialButton(
                  onPressed: () {},
                  minWidth: 24,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      ImageIcon(
                        AssetImage("images/diversity_1_black_24dp 1.png"),
                        size: 24,
                        color: Color.fromRGBO(197, 17, 98, 1.0),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}

Widget getPastille({required StateCheck state, required String text}) {
  Color colorText = Colors.white;
  Color colorBackground = Colors.black;
  Color colorBorder = Colors.black;
  switch (state) {
    case StateCheck.disableCheck:
      colorText = Colors.white;
      colorBackground = const Color.fromARGB(255, 200, 230, 201);
      colorBorder = colorBackground;
      break;
    case StateCheck.disableUncheck:
      colorText = const Color.fromARGB(255, 155, 155, 155);
      colorBackground = const Color.fromARGB(255, 245, 245, 245);
      colorBorder = colorBackground;
      break;
    case StateCheck.enableCheck:
      colorText = Colors.white;
      colorBackground = const Color.fromARGB(255, 76, 175, 80);
      colorBorder = colorBackground;
      break;
    case StateCheck.enableUncheck:
      colorText = Colors.black;
      colorBackground = const Color.fromARGB(255, 245, 245, 245);
      colorBorder = const Color.fromARGB(255, 76, 175, 80);
      break;
  }

  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 8.0),
    child: Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            border: Border.all(color: colorBorder, width: 5),
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            color: colorBackground,
          ),
        ),
        Text(text, style: TextStyle(fontSize: 25, color: colorText, fontWeight: FontWeight.bold))
      ],
    ),
  );
}