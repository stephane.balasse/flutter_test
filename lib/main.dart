import 'dart:math';

import 'package:flutter/material.dart';
import 'package:test/my_bottom_app_bar.dart';

void main() {
  runApp(const MyApp());
}

enum StateCheck { disableCheck, disableUncheck, enableCheck, enableUncheck }

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyTopBar extends SliverPersistentHeaderDelegate {
  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final progress = shrinkOffset / maxExtent;

    return TopBar(
      shrinkOffset: shrinkOffset,
      progress: progress,
    );
  }

  @override
  double get maxExtent => 200;

  @override
  double get minExtent => 85;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) => true;
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: const Color.fromRGBO(128, 222, 234, 1),
      //   title: const Text(''),
      //   shadowColor: Colors.red,
      // ),
      extendBodyBehindAppBar: true,
      backgroundColor: const Color.fromRGBO(178, 235, 242, 1.0),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Color.fromRGBO(250, 250, 250, 1.0), Color.fromRGBO(178, 235, 242, 1.0)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(pinned: true, floating: true, delegate: _MyTopBar()),
            SliverFixedExtentList(
              itemExtent: 50.0,
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    alignment: Alignment.center,
                    // color: Colors.lightBlue[100 * (index % 9)],
                    child: Text('List Item $index'),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: const MyBottomAppBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: const Color.fromARGB(255, 0, 151, 167),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(50.0),
          ),
        ),
        tooltip: 'Increment',
        child: const Icon(
          Icons.edit,
          size: 40,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked, // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class TopBar extends StatelessWidget {
  const TopBar({Key? key, required this.shrinkOffset, required this.progress}) : super(key: key);

  final double shrinkOffset;
  final double progress;

  @override
  Widget build(BuildContext context) {
    final percent = min(shrinkOffset / (200 - 82), 1.0);
    return Stack(children: [
      Positioned(
        top: -300,
        left: -(580 - MediaQuery.of(context).size.width) / 2,
        child: EllipseWidget(
          percent: percent,
          progress: progress,
        ),
      ),
      Positioned(
        top: 90 - (percent * 4 ),
        left: MediaQuery.of(context).size.width / 2 - (45 - (45 * percent)),
        child: AnimatedOpacity(
          duration: const Duration(milliseconds: 200),
          opacity: max(1 - percent, 0),
          child: Container(
            width: 90 - (90 * percent),
            height: 90 - (90 * percent),
            decoration: const BoxDecoration(
              color: Color.fromRGBO(0, 151, 167, 1.0),
              borderRadius: BorderRadius.all(
                Radius.circular(45),
              ),
              boxShadow: [
                BoxShadow(color: Color.fromRGBO(0, 0, 0, .25), blurRadius: 10.0, offset: Offset(0, 6), spreadRadius: 0),
              ],
            ),
            child: Icon(
              Icons.folder,
              size: 50 - (50 * percent),
              color: Colors.white,
            ),
          ),
        ),
      ),
      Positioned(
        top: max(50 - (12 * percent), 40),
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Text(
            'Mon dossier',
            style: Theme.of(context).textTheme.headline4?.copyWith(color: const Color.fromRGBO(233, 30, 99, 1.0), fontWeight: FontWeight.bold),
          ),
        ),
      ),
    ]);
  }
}

class EllipseWidget extends StatelessWidget {
  const EllipseWidget({Key? key, required this.percent, required this.progress}) : super(key: key);

  final double percent;
  final double progress;

  @override
  Widget build(BuildContext context) {
    print(progress);
    print(percent);
    return Stack(
      children: [
        Container(
          width: 576,
          height: 440 - (55 * percent),
          decoration: BoxDecoration(
            boxShadow: const [BoxShadow(color: Color.fromRGBO(248, 187, 208, 0.8), offset: Offset(0, 5), blurRadius: 10)],
            color: const Color.fromRGBO(128, 222, 234, 1),
            borderRadius: BorderRadius.all(Radius.elliptical(576, 440 - (percent * 440))),
          ),
        ),
        Positioned(
          top: 85,
          left: 0,
          child: Container(
            height: 3,
            width: 100,
            decoration: const BoxDecoration(
              color: Colors.black,
              boxShadow: [BoxShadow(color: Color.fromRGBO(248, 187, 208, 0.8), offset: Offset(0, 5), blurRadius: 10)],
            ),
          ),
        ),
      ],
    );
  }
}
